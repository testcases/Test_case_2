from django.contrib import admin
from .models import Category
from mptt.admin import MPTTModelAdmin
from django_mptt_admin.admin import DjangoMpttAdmin

class CategoryAdmin(DjangoMpttAdmin):
    pass

admin.site.register(Category, CategoryAdmin)
