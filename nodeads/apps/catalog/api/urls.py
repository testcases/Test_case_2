from django.conf.urls import url
from apps.catalog.api.views import  CategoryView, MainCategoryView

urlpatterns = [
    url(r'^category/(?P<cat_name>[A-Za-z0-9_]+)/$',CategoryView.as_view()), # category with name
    url(r'^$', MainCategoryView.as_view()), # category with no parent (main categories)
]
